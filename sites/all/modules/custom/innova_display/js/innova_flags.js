/**
 * @file
 * jQuery code.
 * Modifications for Innova Display enhancement
 */


/*
 * JS Behaviors for updating content after flagging 
 *
 */
var _first = true; // Controls first execution on load page
Drupal.behaviors.flag_update = {
  attach: function(context) {
    jQuery(document).one('flagGlobalAfterLinkUpdate', function(event, data) {
    	_holders = new Array();
    	// First time after page load, the event is triggering twice (WTF!) ... Tricked with this boolean
    	if ( !_first ) {
    		// Define selector depending on flag clicked
	    	switch ( data.flagName ) {
	    		case 'follow_idea':
	    			_holders[0] = jQuery(data.link).parents('.node.node-idea').find('.field-name-flag-count-follow-idea div.field-item');
	    			_holders[1] = jQuery('.field-name-flag-usernode-follow-idea div.field-item');
	    			break;
	    		case 'like_idea':
	    			_holders[0] = jQuery(data.link).parents('.node.node-idea').find('.field-name-flag-count-like-idea div.field-item');
	    			_holders[1] = jQuery('.field-name-flag-usernode-like-idea div.field-item');
	    			break;
	    	}
	    	// Update counters
	    	_holders.forEach(function(_holder) {
		    	flagCount = parseInt(_holder.text());
		    	flagCount = ( data.flagStatus == 'flagged' ) ? flagCount + 1 : flagCount - 1;
		    	_holder.text(flagCount);
		    });
	    } else {
	    	_first = false;
	    }
    });
  }

}