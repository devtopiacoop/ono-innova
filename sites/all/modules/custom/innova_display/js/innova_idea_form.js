/**
 * @file
 * jQuery code.
 * Modifications for Innova Display enhancement
 */


/*
 * Helper function: Convert select list into toggled links on tags selector for new idea form
 */
function select_list_into_toggles(_selector,_id) {
	var _selectorOptions = _selector.find('option'); // Better use classes
	var terms = [];

	jQuery("<div id='"+_id+"' class='term-holder' />").insertBefore(_selector);
	_selector.hide();
	_selectorOptions.each(function(i,e){
		if ( jQuery(e).val() != "_none" && jQuery(e).text() != "reto" ) {
			jQuery("#"+_id).append("<div class='views-row'><a href='#'>"+jQuery(e).text().trim()+"</a></div>");
			if(jQuery(e).attr("selected") == true){
				jQuery("#"+_id+" div:last").addClass("selected");
			}
		}
	});	
	//jQuery("#term-holder div").eq(0).remove();
	jQuery("#"+_id).append("<div style='clear:both'/>");
	
	jQuery("#"+_id+" a").click(function(e){
			var text = jQuery(this).text();
			jQuery(_selectorOptions).filter(function() {
			    return jQuery(this).text().trim() == text; 
			}).each(function(i,e){
				jQuery(e).attr('selected', !jQuery(e).attr('selected'));
			});

		jQuery(this).parents(".views-row").toggleClass("selected");
		
		//jQuery("#edit-submit-vista-de-idea").click();
		return false;
	});
}

/*
 * Helper function: Update tags list
 */
function update_tags_list(_tags_selector,_options,_id) {
	// Remove old
	_tags_selector.find('option').remove();
	jQuery('#'+_id).remove();
	// Set new options
	jQuery.each(_options, function(tid, label) {
		_tags_selector.append('<option value="' + tid + '">' + label + '</option>');
	});
	// Convert to toggle
	select_list_into_toggles(_tags_selector,_id);
}

/*
 * JS Behaviors for node/add/idea form 
 *
 */
Drupal.behaviors.idea_add_form_tunes = {
  attach: function(context) {
	var _idea_tags_selector_wrapper = jQuery('.field-name-field-idea-tags');
	var _idea_tags_selector = jQuery('.field-name-field-idea-tags select.form-select'); 
	var _contest_tags_selector_wrapper = jQuery('.field-name-field-contest-tags'); 
	var _contest_tags_selector = jQuery('.field-name-field-contest-tags select.form-select'); 

	var _contest_selector = jQuery('.field-name-field-contest select.form-select'); // Better use classes

	// Convert select list into toggle links
	select_list_into_toggles(_idea_tags_selector,'term-holder-idea');
	select_list_into_toggles(_contest_tags_selector,'term-holder-contest');

	// Hide contest or idea tags selector depending on contest selector
	if ( jQuery('.field-name-field-contest select.form-select option').length===1||jQuery('.field-name-field-contest select.form-select option').filter(function () { return jQuery(this).val() == "_none"; }).attr('selected') ) {
		_contest_tags_selector_wrapper.hide();
	} else {
		_idea_tags_selector_wrapper.hide();
	}

	// On change contest selector > change idea tags
	_contest_selector.change(function(e){
		_selected = jQuery(this).val();
		if ( _selected == "_none" ) {
			// No contest related
			//update_tags_list(_idea_tags_selector,Drupal.settings.innova_idea_tags);
			_idea_tags_selector_wrapper.show();
			_contest_tags_selector_wrapper.hide();
			_contest_tags_selector.find("option:selected").removeAttr("selected");
			// Unselect "reto" option on idea tags selector
			//jQuery(_idea_tags_selector.find('option')).filter(function () { return jQuery(this).text() == "reto"; }).attr('selected',false);
		} else {
			// Contest related > get contest tags
			jQuery.ajax({
			  method: "GET",
			  url: Drupal.settings.basePath + "contest/get/tags/" + _selected,
			})
			  .done(function( data ) {
			  	update_tags_list(_contest_tags_selector,data,'term-holder-contest');
			  	// Hide idea tags selector
			  	_idea_tags_selector_wrapper.hide();
			  	_idea_tags_selector_wrapper.find("option:selected").removeAttr("selected");
			  	// Select "reto" option on idea tags selector
			  	//jQuery(_idea_tags_selector.find('option')).filter(function () { return jQuery(this).text() == "reto"; }).attr('selected',true);
			  	// Show contest tags selector
			  	_contest_tags_selector_wrapper.show();
			  });
		}
	});	

	// Move user author on users selector to top
	jQuery('.field-name-field-related-users div.description').appendTo('#field-related-users-values th.field-label label');

	// Hide Workflow fieldset if is new idea
	if ( Drupal.settings.innova_idea_form == 'add' ) {
		jQuery('.group-idea-workflow').hide();
	} else if( Drupal.settings.innova_idea_form == 'edit' ) {
		// Hide if state != close
		if ( jQuery('.field-name-field-status select.form-select option').filter(function () { return jQuery(this).val() != "3"; }).attr('selected') ) { // 3 = closed
			jQuery('.field-name-field-workflow-closing-reason').hide();
			jQuery('.field-name-field-workflow-closing-area').hide();
		}	
		// On change state
		jQuery('.field-name-field-status select.form-select').change(function(e){
			if ( jQuery(this).val() == 3 ) {
				jQuery('.field-name-field-workflow-closing-reason').show();
				jQuery('.field-name-field-workflow-closing-area').show();
			} else {
				jQuery('.field-name-field-workflow-closing-reason').hide();
				jQuery('.field-name-field-workflow-closing-area').hide();
			}
		});
	}

  }
}
