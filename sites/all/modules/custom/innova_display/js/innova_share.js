/**
 * @file
 * jQuery code.
 * Modifications for Innova Display enhancement
 */


/*
 * JS Behaviors to trigger rule event on click button link of Sharing idea
 *
 */
var _first = true; // Controls first execution on load page
Drupal.behaviors.share_idea = {
  attach: function(context) {
    _link_share_button = jQuery('a.share-idea-button');

	// On click share idea button
	_link_share_button.click(function(e){
			// Trigger event
			jQuery.ajax({
			  method: "GET",
			  url: Drupal.settings.basePath + "innova/share-idea-reaction/" + Drupal.settings.innova_share_idea + "/" + Drupal.settings.innova_share_user,
			})
	});	

  }

}