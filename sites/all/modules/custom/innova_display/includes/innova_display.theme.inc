<?php

/**
 * @file
 * Provides the theming functions for innova displays
 */

/**
 * Display author block for idea from unique user
 */
function theme_innova_user_followed_followers($variables) {
  $output = '<div class="user-followers-followed">';
  $output .= '  <span class="user-followed">' . t('Sigo a <span class="count">!count</span>', array('!count' => $variables['followed'])) . '</span>';
  $output .= '  <span class="user-followers">' . t('Me siguen <span class="count">!count</span>', array('!count' => $variables['followers'])) . '</span>';
  $output .= '</div>';

  return $output;
}

/**
 * Display author block for idea from unique user
 */
function theme_innova_display_author_idea_user($variables) {
  $output = '<div class="author-idea-block one-user">';
  $output .= '  <span class="karma-ico karma_' . $variables['data']['karma_key'] . '"></span>';
  $output .= $variables['data']['picture'];
  $output .= $variables['data']['name'];
  $output .= '  <span class="karma-label karma_' . $variables['data']['karma_key'] . '">' . $variables['data']['karma_label'] . '</span>';
  $output .= '</div>';

  return $output;
}

/**
 * Display author block for idea from group
 */
function theme_innova_display_author_idea_group($variables) {
  $output = '<div class="author-idea-block multi-user">';

  // Add toggle link only on teaser
  $output .= '<span class="multi-user-header">' . t('Idea grupal') . '</span>';

  if ($variables['formatter'] == 'teaser') {
    $output .= '<span class="multi-user-toggle"><a href="" title="' . t('Ver miembros') . '">' . t('Ver miembros') . '</a></span>';
  }

  $output .= '</div>';
  $output .= '<div class="author-idea-members">';
  $output .= $variables['data']['members'];
  $output .= '</div>';
  return $output;
}

/**
 * Display author block for idea from group
 */
function theme_innova_contest_addidea_link($variables) {
  return l(
    t('Tengo una idea'), 'node/add/idea', array(
    'query' => array('contest' => $variables['contest_nid']),
    'arguments' => array('class' => 'add-idea-to-contest-link')
  ));
}

/**
 *  Theme for innova_ideas_states_home
 *  $variables      => total_ideas
 *                  => total_ideas_by_status => SID => array ( 'n_ideas' , 'label' )
 *  variable_get's  => block_states_summary_title_total
 *                  => block_states_summary_enabled_total
 *                  => block_states_summary_text_total
 *                  => block_states_summary_image_total
 *                  => block_states_summary_title_SID
 *                  => block_states_summary_enabled_SID
 *                  => block_states_summary_text_SID
 *                  => block_states_summary_image_SID
 */
function theme_innova_ideas_states_summary($variables) {
  $output = '';
  $output .= '<ul>';

  // First: total account
  if (variable_get('block_states_summary_enabled_total', 1)) {
    $output .= '  <li class="ideas-summary-state-item">';
    if (variable_get('block_states_summary_image_total', 0)) {
      // Theme image
      $file       = file_load(variable_get('block_states_summary_image_total'));
      $image_sets = array(
        'style_name' => 'thumbnail',
        'path' => $file->uri,
      );
      $output .= '    <span class="ideas-summary-state-item-image">' . theme('image_style', $image_sets) . '</span>';
    }
    $output .= '      <span class="ideas-summary-state-item-count">' . $variables['total_ideas'] . '</span>';
    $output .= '      <span class="ideas-summary-state-item-title">' . variable_get('block_states_summary_title_total', '') . '</span>';
    $output .= '      <span class="ideas-summary-state-item-text">' . variable_get('block_states_summary_text_total', '') . '</span>';

    $output .= '  </li>';
  }

  // Then states enabled
  foreach ($variables['total_ideas_by_status'] as $sid => $data) {
    if (variable_get('block_states_summary_enabled_' . $sid, 1)) {
      $output .= '  <li class="ideas-summary-state-item">';
      if (variable_get('block_states_summary_image_' . $sid, 0)) {
        // Theme image
        $file       = file_load(variable_get('block_states_summary_image_' . $sid));
        $image_sets = array(
          'style_name' => 'thumbnail',
          'path' => $file->uri,
        );
        $output .= '    <span class="ideas-summary-state-item-image">' . theme('image_style', $image_sets) . '</span>';
      }
      $output .= '      <span class="ideas-summary-state-item-count">' . $data['n_ideas'] . '</span>';
      $output .= '      <span class="ideas-summary-state-item-title">' . variable_get('block_states_summary_title_' . $sid, '') . '</span>';
      $output .= '      <span class="ideas-summary-state-item-text">' . variable_get('block_states_summary_text_' . $sid, '') . '</span>';

      $output .= '  </li>';
    }
  }
  $output .= '</ul>';

  // Link button
  if (variable_get('block_states_summary_link_url', 0)) {
    $output .= l(variable_get('block_states_summary_link_text', t('Link')), variable_get('block_states_summary_link_url'));
  }

  return $output;
}

/**
 *  Theme for innova_contest_header
 *  $variables      => data
 *                  => image
 *                  => nid
 *                  => open
 *                  => title
 *                  => text
 *                  => date
 *                  => image
 */
function theme_innova_contest_header($variables) {
  $output = '';
  $output .= '<div class="innova-contest-header ' . ($variables['data']['open'] ? ' contest-open ' : ' contest-closed ') . '">';
  $output .= '  <div class="contest-status">';
  $output .= '    <h3>' . t('Reto !status', array('!status' => ($variables['data']['open']) ? t('abierto') : t('cerrado'))) . '</h3>';

  // Closed
  if (!$variables['data']['open']) {
    $output .= '    <span class="contest-date">' . t('Este reto finalizó el !date', array('!date' => format_date($variables['data']['date'], 'date_contest_closed'))) . '</span>';
  }

  $output .= '  </div>';
  $output .= '  <div class="contest-data">';
  $output .= '    <h2>' . l($variables['data']['title'], url('node/' . $variables['data']['nid'], array('absolute' => TRUE))) . '</h2>';
  $output .= '    <p>' . $variables['data']['text'] . '</p>';

  if ($variables['data']['open']) { // Open
    $output .= '<span class="contest-date">' . t('Finaliza el <strong>!date</strong>', array('!date' => format_date($variables['data']['date'], 'date_contest_open'))) . '</span>';

    if(arg(1) !== $variables['data']['nid']){
      $output .= '<div class="innova-button innova-button-purple">';
      $output .= l(t('Conocer el reto'), 'node/' . $variables['data']['nid']);
      $output .= '</div>';
    }

    $output .= '<div class="innova-button innova-button-purple last-of-type">';
    $output .= theme('innova_contest_addidea_link', array('contest_nid' => $variables['data']['nid']));
    $output .= '</div>';
  }

  $output .= '  </div>';
  $output .= '</div>';

  return $output;
}









