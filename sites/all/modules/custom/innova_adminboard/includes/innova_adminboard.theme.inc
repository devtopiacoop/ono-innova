<?php

/**
 * @file
 * Provides the theming functions for innova adminboard
 */

/**
 * Display box block for dashboard
 */
function theme_innova_adminboard_box($variables) {
  $output = '<div class="dashboard-block block-comments">';
  foreach ( $variables['params'] as $param ) {
    $output .= '<div class="wrapper-admin-data">';
    $output .= '  <label>' . $param['label'] . '</label>';
    $output .= '  <span class="data">' . $param['value'] . '</span>';
    $output .= '</div>';
  }
  $output .= '</div>';
  return $output;
}
