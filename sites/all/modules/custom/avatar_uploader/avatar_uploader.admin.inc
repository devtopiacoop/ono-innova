<?php

/**
 * @file
 * Contains the administrative functions of the Avatar Uploader module.
 *
 * This file includes the
 * settings form.
 *
 * @ingroup Avatar Uploader
 */

/**
 * Menu callback for the Avatar Uploader  module settings form.
 *
 * @ingroup forms
 */
function au_settings() {

  $form['settings']['au_selectors'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS Selector'),
    '#default_value' => variable_get('au_selectors',''),
    '#description' => t('Set the selector, for which apply Avatar Uploader interface') . '<br />' .
                      t('For example: .profile .user-picture a'),
    '#disabled' => !user_access('administer settings avatar uploader'),
  );

  return system_settings_form($form);
}
