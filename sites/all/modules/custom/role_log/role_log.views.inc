<?php

/**
 * @file
 * Role Expire Views hooks
 *
 * Views module integration with the role log module. Exposes the following
 * to Views:
 *  Fields:
 *    "Role assignment date/time" - the date/time that a role expires
 *    "Role assignment role" - the role that expires at the given Role assignment date/time
 *  Filters:
 *    "Role assignment date/time"
 *    "Role assignment role"
 *  Arguments (aka "Contextual Filters"):
 *    "Role assignment role" - on the querystring as a role ID, not as a role name.
 *
 * NOTE: The Views API hook hook_views_api must be defined in the main module.
 * @see role_log_views_api() in role_log.module.
 */


/**
 * Implementation of hook_views_data().
 *
 * The purpose of this hook is to tell Views what data we make available.
 */
function role_log_views_data() {
  $data['role_log']['table']['group']  = t('Role Log');

  $data['role_log']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );

  // Expose the role assignment date
  $data['role_log']['timestamp'] = array(
    'title' => t('Role assignment date/time'),
    'help' => t('Date and time the role was assigned.'),
    'field' => array(
      'handler' => 'role_log_handler_field_timestamp',
      'click sortable' => TRUE,
    ),
    // Information for accepting a rid as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // Expose the role id from role_log
  $data['role_log']['rid'] = array(
    'title' => t('Role assignment role'),
    'help' => t('The Role was assigned on date/time'),
    // Information for displaying the rid
    'field' => array(
      'handler' => 'role_log_handler_field_rid',
      'click sortable' => TRUE,
    ),
    // Information for accepting a rid as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_users_roles_rid',
      'name field' => 'title', // the field to display in the summary.
      'numeric' => TRUE,
      'validate type' => 'rid',
    ),
    // Information for accepting a uid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_user_roles',
    ),
    // Information for sorting on a uid.
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );


  $data['role_log']['compare'] = array(
   'title' => t('Check date created'),
    'help' => t('Check node date created to get content created after logged user was assigned with a specific role (f.ex. committee).'),
    'filter' => array(
      'handler' => 'role_log_handler_field_compare',
    ),
  );

  return $data;
}

class role_log_handler_field_rid extends views_handler_field {

  // Derived from views_handler_field_user_roles
  // Purpose: get the *names* that correspond to the role_log_rids.
  function pre_render(&$values) {
    $roles = array();
    $this->items = array();

    // Get all the unique role ids into the keys of $roles. Initializing into
    // array_keys helps prevent us from having a list where the same rid appears
    // over and over and over.
    foreach ($values as $result) {
      $roles[$this->get_value($result, NULL, TRUE)] = FALSE;
    }

    if ($roles) {
      $result = db_query("SELECT r.rid, r.name FROM {role} r WHERE r.rid IN (:rids) ORDER BY r.name",
        array(':rids' => array_keys($roles)));
      foreach ($result as $role) {
        $this->items[$role->rid]['role'] = check_plain($role->name);
        $this->items[$role->rid]['rid'] = $role->rid;
      }
    }
  }

  // Render the rid as the role name.
  function render($values) {

    // Return the role name corresponding to the role ID.
    // TODO: Should I be using this->get_value() here?
    $rid = $values->role_log_rid;
    if ($rid) {
      $role = $this->items[$rid]['role'];
      if (!empty($role)) {
        return $role;
      }
    }
  }
}

class role_log_handler_field_timestamp extends views_handler_field {


  function option_definition() {
    $options = parent::option_definition();

    $options['roles'] = array('default' => '');

    return $options;
  }

  /**
   * Build the options form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form,$form_state);

    $form['roles'] = array(
      '#type' => 'select',
      '#title' => t('Role to filter'),
      '#description' => t('Select the role you want the date/time log for.'),
      '#default_value' => $this->options['roles'],
      '#options' => user_roles(),
      '#attributes' => array('class' => array())
    );
  }

  /**
   * Perform any necessary changes to the form values prior to storage.
   * There is no need for this function to actually store the data.
   */
  function options_submit(&$form, &$form_state) {

    $this->options['roles'] = $form['values']['options']['roles'];
    $this->options['attributes']['class'] = 'active';
    parent::options_submit($form,$form_state);
  }

  // Render the rid as the role name.
  function render($values) {
    $ts = role_log_get_user_role_last_assignment_time($values->uid,$this->options['roles']);
    if ( $ts != '' ) 
      return date("Y-m-d H:i:s",$ts); 
    else
      return t('-');
  }

}

class role_log_handler_field_compare extends views_handler_filter_string {
  /**
   * @var views_plugin_query_default
   */
  public $query;

  function option_definition() {
    $options = parent::option_definition();
    $options['fields'] = array('default' => array());
    $options['roles'] = array('default' => array());

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $this->view->init_style();

    // Allow to choose one field to compare
    if ($this->view->style_plugin->uses_fields()) {
      $options = array();
      foreach ($this->view->display_handler->get_handlers('field') as $name => $field) {
        $options[$name] = $field->ui_name(TRUE);
      }
      if ($options) {
        $form['fields'] = array(
          '#type' => 'select',
          '#title' => t('Choose one field to compare with logged user role timestamp assignment'),
          '#description' => t("This filter doesn't work for very special field handlers."),
          '#multiple' => false,
          '#options' => $options,
          '#default_value' => $this->options['fields'],
        );
      }
      else {
        form_set_error('', t('You have to add one fields to be able to use this filter.'));
      }
    }

    // Allow to chose one role
    $options = user_roles();
    if ($options) {
      $form['roles'] = array(
        '#type' => 'select',
        '#title' => t('Choose one role to check last assignment'),
        '#multiple' => false,
        '#options' => $options,
        '#default_value' => $this->options['roles'],
      );
    }
    else {
      form_set_error('', t('You have to add one role to be able to use this filter.'));
    }
  }

  function query() {
    global $user;
    $this->view->_build('field');
    $fields = array();
    // Only add the fields if they have a proper field and table alias.
    $field = $this->view->field[$this->options['fields']];
    $role = $this->options['roles'];
   
    $date_to_compare = role_log_get_user_role_last_assignment_time($user->uid,$role);
    if ( $date_to_compare == '' ) { $date_to_compare = time(); } // user logged has not the role
    if ( $user->uid == 1 ) { $date_to_compare = 0; } // user admin can access all content
    $expression = "$field->table_alias.$field->real_field" . " >= " . $date_to_compare;

    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      $this->{$info[$this->operator]['method']}($expression);
    }
    
  }

  // By default things like op_equal uses add_where, that doesn't support
  // complex expressions, so override all operators.
  function op_equal($field) {
    $placeholder = $this->placeholder();
    $operator = $this->operator();
    $this->query->add_where_expression($this->options['group'], $field);
  }

}