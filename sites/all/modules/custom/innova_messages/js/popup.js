/**
 * @file
 * jQuery code.
 * Based on code: Adrian "yEnS" Mato Gondelle, twitter: @adrianmg
 * Modifications for Drupal: Grzegorz Bartman grzegorz.bartman@openbit.pl
 */


// Setting up popup.
// 0 means disabled; 1 means enabled.
var popupStatus = 0;

/**
 * Loading popup with jQuery.
 */
function popup_message_load_popup() {
  // Loads popup only if it is disabled.
  //if (popupStatus == 0) {
    jQuery(".popup-message-background").css( {
      "opacity": "0.7"
    });
    jQuery(".popup-message-background").fadeIn("slow");
    jQuery(".popup-message-window").fadeIn("slow");
    popupStatus = 1;
  //}
}

/**
 * Disabling popup with jQuery.
 */
function popup_message_disable_popup(p) {
  // Disables popup only if it is enabled.
  //if (p.popupStatus == 1) {
    p.fadeOut("slow", function(){ 
        jQuery(this).remove();
        if ( jQuery(".popup-message-window").length == 0 ) {
          jQuery(".popup-message-background").fadeOut("slow", function(){ 
            jQuery(this).remove();
          });
        }
    });
    // _hideBack = true;
    // jQuery(".popup-message-window").each(function(){
    //   if ( jQuery(this).is(':visible') && _hideBack ) {
    //     _hideBack = false;
    //   }
    // });
    // if ( _hideBack ) {
    //   p.next().fadeOut("slow"); // background
    // }
    p.popupStatus = 0;
  //}
}

/**
 * Centering popup.
 */
function popup_message_center_popup(width, height) {
  // Request data for centering.
  var windowWidth = document.documentElement.clientWidth;
  var windowHeight = document.documentElement.clientHeight;

  var popupWidth = 0
  if (typeof width == "undefined") {
    popupWidth = $(".popup-message-window").width();
  }
  else {
    popupWidth = width;
  }
  var popupHeight = 0
  if (typeof width == "undefined") {
    popupHeight = $(".popup-message-window").height();
  }
  else {
    popupHeight = height;
  }

  // Centering.
  jQuery(".popup-message-window").css( {
    "position": "absolute",
    "width" : popupWidth + "px",
    "height" : popupHeight + "px",
    "top": windowHeight / 2 - popupHeight / 2,
    "left": windowWidth / 2 - popupWidth / 2,
    "z-index": 100002
  });
  // Only need force for IE6.
  jQuery(".popup-message-background").css( {
    "height": windowHeight,
    "opacity": 0.7,
    "background": "#000",
    "width": "100%",
    "z-index": 100000,
    "position": "absolute",
    "top": 0,
    "left": 0
  });

}

/**
 * Display popup message.
 */
function popup_message_display_popup(popup_message_title, popup_message_body, width, height) {
  jQuery('body').append('<div class="popup-message-window"><a class="popup-message-close">X</a>\n\
    <h1 class="popup-message-title">' + popup_message_title + '</h1><div class="popup-message-content">' + popup_message_body
    + '</div></div>');

  if ( jQuery('.popup-message-background').length == 0 ) { 
    jQuery('body').append('<div class="popup-message-background"></div>');
  }

  // Loading popup.
  popup_message_center_popup(width, height);
  popup_message_load_popup();

  // Closing popup.
  // Click the x event!
  jQuery(".popup-message-close").click(function() {
    popup_message_disable_popup(jQuery(this).parent());
  });
  // Click out event!
  jQuery(".popup-message-background").click(function() {
    popup_message_disable_popup(jQuery('.popup-message-window').last());
  });
  // Press Escape event!
  jQuery(document).keypress(function(e) {
    if (e.keyCode == 27 && popupStatus == 1) {
      popup_message_disable_popup(jQuery(this));
    }
  });
}

/**
 * Helper function for get last element from object.
 * Used if on page is loaded more than one message.
 */
function popup_message_get_last_object_item(variable_data) {
  if (typeof(variable_data) == 'object') {
      variable_data = variable_data[(variable_data.length - 1)];
  }
  return variable_data;
}

Drupal.behaviors.popup_message = {
  attach: function(context) {

    jQuery.each(Drupal.settings.innova_messages, function(nid, nodedata) {
        var check_cookie = true;
        if ( nodedata.always ) {
          // Check session cookie
          var cookiedata = "popup_message_displayed_session_" + nodedata.session + "_" + nodedata.nid;
          check_cookie = popup_message_get_last_object_item(check_cookie);
          var popup_message_cookie = jQuery.cookie(cookiedata);
        } else {
          // Check user cookie
          var cookiedata = "popup_message_displayed_" + nodedata.uid + "_" + nodedata.nid;
          check_cookie = popup_message_get_last_object_item(check_cookie);
          var popup_message_cookie = jQuery.cookie(cookiedata);
        }
        
        
        if ( popup_message_cookie != 1 || check_cookie == false) {
          // Set cookie.
          jQuery.cookie(cookiedata, 1, {
            path: '/'
          });

          // Get variables.
          var popup_message_title = nodedata.title;
          var popup_message_body = nodedata.body;
          var popup_message_width = nodedata.width;
          var popup_message_height = nodedata.height;
          popup_message_title = popup_message_get_last_object_item(popup_message_title);
          popup_message_body = popup_message_get_last_object_item(popup_message_body);
          popup_message_width = popup_message_get_last_object_item(popup_message_width);
          popup_message_height = popup_message_get_last_object_item(popup_message_height);
          // Display message.
          popup_message_display_popup(popup_message_title, popup_message_body,
            popup_message_width, popup_message_height)
        }
    });
    
  }
};
