<?php

function migrate_onoinnova_drush_command() {
  $items = array();

  $items['migrate_onoinnova-import-flags'] = array(
    'description' => dt('Run import patch.'),
    'callback' => '_migrate_onoinnova_import_flags'
  );
  $items['migrate_onoinnova-import-workflow'] = array(
    'description' => dt('Run import patch.'),
    'callback' => '_migrate_onoinnova_import_workflow'
  );
  $items['migrate_onoinnova-import-userpoints'] = array(
    'description' => dt('Run import patch.'),
    'callback' => '_migrate_onoinnova_import_userpoints'
  );
  $items['migrate_onoinnova-import-areareason'] = array(
    'description' => dt('Run import patch.'),
    'callback' => '_migrate_onoinnova_import_areareason'
  );
  $items['migrator-update-ideagroup'] = array(
    'description' => dt('Run import patch.'),
    'callback' => '_migrate_onoinnova_update_ideagroup'
  );
  $items['migrator-set-roles'] = array(
    'description' => dt('Run import patch.'),
    'callback' => '_migrate_onoinnova_set_roles'
  );
  $items['innova-set-suscriptions-on'] = array(
    'description' => dt('Set suscriptions on for all users.'),
    'callback' => '_innova_set_suscriptions_on'
  );
  $items['innova-set-suscriptions-off'] = array(
    'description' => dt('Set suscriptions off for all users.'),
    'callback' => '_innova_set_suscriptions_off'
  );

  return $items;
}

?>