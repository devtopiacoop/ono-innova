<?php

/**
 * @file
 * Idea classes declaration for migration process
 */

/**
 * User pictures migration
 */
class NewsVPicturesMigration extends IdeasMigration {


   public function __construct($arguments) {
    parent::__construct($arguments);

      $this->description = t('News pictures images');

      // Database source connection
      $query = Database::getConnection('default', 'ono_innova_db')
              ->select('field_data_field_imagen', 'fdfi');
      $query->innerjoin('file_managed','fm','fm.fid=fdfi.field_imagen_fid');
      $query->fields('fm', array('uri'))
            ->fields('fdfi', array('field_imagen_fid'))
              ->isNotNull('field_imagen_fid')
              ->condition('fdfi.bundle', 'news', '=');

      $this->source = new MigrateSourceSQL($query, array(), null, array('map_joinable' => false));

      $this->destination = new MigrateDestinationFile();

      $this->map = new MigrateSQLMap($this->machineName,
                      array('field_imagen_fid' => array(
                              'type' => 'int',
                              'length' => 10,
                              'not null' => FALSE,
                              'description' => 'Image fid.'
                      )),
                      MigrateDestinationFile::getKeySchema());
      $this->addFieldMapping('destination_dir')
      ->defaultValue(variable_get('file_default_scheme', 'public') . '://news/vertical');

      // Just map the incoming URL to the destination's 'uri'
      $this->addFieldMapping('value', 'uri');
      $this->addFieldMapping('preserve_files')
            ->defaultValue(false);
      $this->addFieldMapping('file_replace')
            ->defaultValue(FILE_EXISTS_REPLACE);
      $this->addUnmigratedDestinations(array('fid', 'uid', 'path', 'destination_file','source_dir','urlencode','timestamp'));
   }

}
// class NewsHPicturesMigration extends IdeasMigration {


//    public function __construct($arguments) {
//     parent::__construct($arguments);

//       $this->description = t('News pictures images');

//       // Database source connection
//       $query = Database::getConnection('default', 'ono_innova_db')
//               ->select('field_data_field_imagen_horizontal', 'fdfi');
//       $query->innerjoin('file_managed','fm','fm.fid=fdfi.field_imagen_horizontal_fid');
//       $query->fields('fm', array('uri'))
//             ->fields('fdfi', array('field_imagen_horizontal_fid'))
//               ->isNotNull('field_imagen_horizontal_fid')
//               ->condition('fdfi.bundle', 'news', '=');

//       $this->source = new MigrateSourceSQL($query, array(), null, array('map_joinable' => false));

//       $this->destination = new MigrateDestinationFile();

//       $this->map = new MigrateSQLMap($this->machineName,
//                       array('field_imagen_horizontal_fid' => array(
//                               'type' => 'int',
//                               'length' => 10,
//                               'not null' => FALSE,
//                               'description' => 'Image fid.'
//                       )),
//                       MigrateDestinationFile::getKeySchema());

//       $this->addFieldMapping('destination_dir')
//       ->defaultValue(variable_get('file_default_scheme', 'public') . '://news/horizontal');

//       // Just map the incoming URL to the destination's 'uri'
//       $this->addFieldMapping('value', 'uri');
//       $this->addFieldMapping('preserve_files')
//             ->defaultValue(false);
//       $this->addFieldMapping('file_replace')
//             ->defaultValue(FILE_EXISTS_REPLACE);
//       $this->addUnmigratedDestinations(array('fid', 'uid', 'path', 'destination_file','source_dir','urlencode','timestamp'));
//    }

// }

/**
 * Aabstract intermediate class derived from Migration for sharing settings, utility functions, etc.
 */
abstract class NewsMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    // People involved in the particular migration, with their role and contact info
    $this->team = array(
      new MigrateTeamMember('Jose Ibanez', 'jose.ibanez@tecnilogica.com',
                            t('Migration Master'))
    );

    // Old database connection
    Database::addConnectionInfo('ono_innova_db', 'default', array(
      'driver'    => variable_get( 'OLD_DB_TYPE' ),
      'database'  => variable_get( 'OLD_DB_NAME' ),
      'username'  => variable_get( 'OLD_DB_USER' ),
      'password'  => variable_get( 'OLD_DB_PASS' ),
      'host'      => variable_get( 'OLD_DB_HOST' ),
      'prefix'    => '',
    ));

  }
}



/**
 * Idea node migration
 */
class NewsNodeMigration extends NewsMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('News from Innova');


    // Database source connection
    $query = Database::getConnection('default', 'ono_innova_db')
              ->select('node', 'n');
    $query->leftjoin('field_data_body', 'fdb', 'fdb.entity_id = n.nid');
    $query->leftjoin('field_data_field_imagen', 'fdfi', 'fdfi.entity_id = n.nid');
    $query->leftjoin('field_data_field_imagen_horizontal', 'fdfih', 'fdfih.entity_id = n.nid');
    $query->fields('n', array('nid','title','uid','status','created','changed','comment','sticky'))
          ->fields('fdb', array('body_value','body_summary'))
          ->fields('fdfi', array('field_imagen_fid'))
          ->fields('fdfih', array('field_imagen_horizontal_fid'))
          //->fields('wn', array('sid'))
          ->condition('type', 'news', '=');

    $this->source = new MigrateSourceSQL($query);

    // Set up destination - nodes of type idea
    $this->destination = new MigrateDestinationNode('news');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'nid' => array(
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'Idea ID.',
          'alias' => 'n'
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // Mapped fields
    $this->addSimpleMappings(array('nid','title','status','created','changed','comment','sticky'));


    // References to users
    $this->addFieldMapping('uid', 'uid')
        ->sourceMigration('IdeaUser');

    $this->addFieldMapping('body', 'body_value');
    $this->addFieldMapping('body:summary', 'body_summary');
    $this->addfieldMapping('body:format')->defaultValue('full_html');

    // File mapping
    $this->addFieldMapping('field_image', 'field_imagen_fid')
     ->sourceMigration('NewsVPictures');
     $this->addFieldMapping('field_image:file_class')
     ->defaultValue('MigrateFileFid');

    // $this->addFieldMapping('field_image_horizontal', 'field_imagen_horizontal_fid')
    //  ->sourceMigration('NewsHPictures');
    //       $this->addFieldMapping('field_image_horizontal:file_class')
    //  ->defaultValue('MigrateFileFid');

    $this->addFieldMapping('is_new') -> defaultValue('1');

    // Unmapped destination fields
    $this->addUnmigratedDestinations(array(
      'promote',
      'revision',
      'log',
      'language',
      'tnid',
      'revision_uid',
      'body:language',
      'field_tags',
      'field_tags:source_type',
      'field_tags:create_term',
      'field_tags:ignore_case',
      'translate',
    ));

    $destination_fields = $this->destination->fields();

    if (isset($destination_fields['path'])) {
      $this->addFieldMapping('path')
           ->issueGroup(t('DNM'));
      if (isset($destination_fields['pathauto'])) {
        $this->addFieldMapping('pathauto')
             ->issueGroup(t('DNM'));
      }
    }

  }

}



/**
 * Import items from the migrate_example_beer_comment table and make them into
 * Drupal comment objects.
 */
class NewsCommentMigration extends NewsMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = 'Comments about news';

    // Database source connection
    $query = Database::getConnection('default', 'ono_innova_db')
            ->select('comment', 'c');

    $query->leftjoin('node', 'n', 'n.nid = c.nid');
    $query->leftjoin('field_data_comment_body', 'fdcb', 'fdcb.entity_id = c.nid');
   
    $query->fields('c', array('cid', 'pid', 'nid', 'uid', 'subject', 'hostname', 'created', 'changed','status','thread','name','mail'))
          ->fields('fdcb', array('comment_body_value'))
          ->condition('n.type', 'news', '=')
          ->orderBy('pid', 'ASC');


    $this->source = new MigrateSourceSQL($query);
    // Note that the machine name passed for comment migrations is
    // 'comment_node_' followed by the machine name of the node type these
    // comments are attached to.
    $this->destination =
      new MigrateDestinationComment('comment_node_news');

    $this->map = new MigrateSQLMap($this->machineName,
      array('cid' => array(
            'type' => 'int',
            'not null' => TRUE,
           )
         ),
      MigrateDestinationComment::getKeySchema()
    );

    // Mapped fields
    $this->addSimpleMappings(array('created','changed', 'subject', 'name','mail','hostname','thread'));
    $this->addFieldMapping('status')
         ->defaultValue(COMMENT_PUBLISHED);

    $this->addFieldMapping('nid', 'nid')
         ->sourceMigration('NewsNode');

    $this->addFieldMapping('uid', 'uid')
         ->sourceMigration('IdeaUser')
         ->defaultValue(0);

    $this->addFieldMapping('pid', 'pid')
         ->sourceMigration('NewsCommentMigration')
         ->description('Parent comment.');

    $this->addFieldMapping('comment_body', 'comment_body_value');

    // No unmapped source fields

    // Unmapped destination fields
    $this->addUnmigratedDestinations(array(
      'comment_body:format', 'comment_body:language',
      'homepage',
      'language',
    ));

    $destination_fields = $this->destination->fields();
    if (isset($destination_fields['path'])) {
      $this->addFieldMapping('path')
           ->issueGroup(t('DNM'));
      if (isset($destination_fields['pathauto'])) {
        $this->addFieldMapping('pathauto')
             ->issueGroup(t('DNM'));
      }
    }
  }
}
