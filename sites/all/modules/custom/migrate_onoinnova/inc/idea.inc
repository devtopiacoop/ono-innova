<?php

/**
 * @file
 * Idea classes declaration for migration process
 */

/**
 * Aabstract intermediate class derived from Migration for sharing settings, utility functions, etc.
 */
abstract class IdeasMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    // People involved in the particular migration, with their role and contact info
    $this->team = array(
      new MigrateTeamMember('Jose Ibanez', 'jose.ibanez@tecnilogica.com',
                            t('Migration Master'))
    );

    // Old database connection
    Database::addConnectionInfo('ono_innova_db', 'default', array(
      'driver'    => variable_get( 'OLD_DB_TYPE' ),
      'database'  => variable_get( 'OLD_DB_NAME' ),
      'username'  => variable_get( 'OLD_DB_USER' ),
      'password'  => variable_get( 'OLD_DB_PASS' ),
      'host'      => variable_get( 'OLD_DB_HOST' ),
      'prefix'    => '',
    ));

  }
}

/**
 * Idea Term Migration
 */
class IdeaTermMigration extends IdeasMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description =
      t('Migrate styles from the source database to taxonomy terms');

    // Database source connection
    $query = Database::getConnection('default', 'ono_innova_db')
              ->select('taxonomy_term_data', 'ttd')
              ->fields('ttd', array('tid','vid','name','description'))
              ->condition('ttd.vid', 3, '=');;

    // Create a MigrateSource object, which manages retrieving the input data.
    $this->source = new MigrateSourceSQL($query);

    // Set up destination - machine name of the vocabulary
    $this->destination =
      new MigrateDestinationTerm('tags_idea');

    // Create a map object for tracking the relationships between source rows
    // and their resulting Drupal objects
    $this->map = new MigrateSQLMap(
        // 1. The machine name (IdeaTerm) of this migration
        $this->machineName,
        // 2. Schema definition for the primary keys of the source
        array(
          'tid' => array(
              'type' => 'int',
              'length' => 10,
              'not null' => FALSE,
              'description' => 'Term ID',
              )
        ),
        // 3. Schema definition for the primary keys of the destination
        MigrateDestinationTerm::getKeySchema()
      );
    $this->addFieldMapping('name', 'name');

    $this->addFieldMapping(NULL, 'description')
         ->description(t('This info will not be maintained in new Drupal'))
         ->issueGroup(t('DNM'));

    $this->addFieldMapping('format')
         ->issueGroup(t('DNM'));
    $this->addFieldMapping('weight')
         ->issueGroup(t('DNM'));
    $this->addFieldMapping('parent')
         ->issueGroup(t('DNM'));
    $this->addFieldMapping('parent_name')
         ->issueGroup(t('DNM'));

    // We conditionally DNM these fields, so your field mappings will be clean
    // whether or not you have path and/or pathauto enabled.
    $destination_fields = $this->destination->fields();
    if (isset($destination_fields['path'])) {
      $this->addFieldMapping('path')
           ->issueGroup(t('DNM'));
      if (isset($destination_fields['pathauto'])) {
        $this->addFieldMapping('pathauto')
             ->issueGroup(t('DNM'));
      }
    }
  }
}

/**
 * User pictures migration
 */
class UserPicturesMigration extends IdeasMigration {


   public function __construct($arguments) {
    parent::__construct($arguments);

      $this->description = t('User pictures images');

      // Database source connection
      $query = Database::getConnection('default', 'ono_innova_db')
              ->select('users', 'u');
      $query->innerjoin('file_managed','fm','fm.fid=u.picture');
      $query->fields('fm', array('uri'))
            ->fields('u', array('picture'))
              ->isNotNull('picture')
              ->condition('picture', '', '!=')
              ->condition('picture', '0', '>');

      $this->source = new MigrateSourceSQL($query, array(), null, array('map_joinable' => false));

      $this->destination = new MigrateDestinationFile();

      $this->map = new MigrateSQLMap($this->machineName,
                      array('picture' => array(
                              'type' => 'int',
                              'length' => 10,
                              'not null' => FALSE,
                              'description' => 'Image fid.'
                      )),
                      MigrateDestinationFile::getKeySchema());


      // Just map the incoming URL to the destination's 'uri'
      $this->addFieldMapping('value', 'uri')
            ->defaultValue('public://pictures/img-user.png');
      $this->addFieldMapping('preserve_files')
            ->defaultValue(false);
      $this->addFieldMapping('file_replace')
            ->defaultValue(FILE_EXISTS_REPLACE);
      $this->addUnmigratedDestinations(array('fid', 'uid', 'path', 'destination_dir', 'destination_file','source_dir','urlencode','timestamp'));
   }

}

/**
 * Idea user migration
 */
class IdeaUserMigration extends IdeasMigration {
  public function __construct($arguments) {
    // The basic setup is similar to IdeaTermMigraiton
    parent::__construct($arguments);
    $this->description = t('Innova users');

    // Database source connection
    $query = Database::getConnection('default', 'ono_innova_db')
              ->select('users', 'u');
    $query->leftjoin('field_data_field_apellidos', 'fdfa', 'fdfa.entity_id = u.uid');
    $query->leftjoin('field_data_field_nombre', 'fdfn', 'fdfn.entity_id = u.uid');
    $query->leftjoin('field_data_field_karma', 'fdfk', 'fdfk.entity_id = u.uid');
    $query->leftjoin('field_data_field_ou', 'fdfo', 'fdfo.entity_id = u.uid');
    $query->fields('u', array('uid','name','pass','mail','created','access','login','status','picture','init'))
          ->fields('fdfa', array('field_apellidos_value'))
          ->fields('fdfn', array('field_nombre_value'))
          ->fields('fdfk', array('field_karma_value'))
          ->fields('fdfo', array('field_ou_value'))
          ->condition('u.uid', 1, '>');
    $this->source = new MigrateSourceSQL($query);

    // Destination
    $this->destination = new MigrateDestinationUser();

    // Map object
    $this->map = new MigrateSQLMap(
        $this->machineName,
        array('uid' => array(
                'type' => 'int',
                'not null' => TRUE,
                'description' => 'User ID.'
                )
              ),
        MigrateDestinationUser::getKeySchema()
    );

    // Mapped fields
      // Simple mapping
      $this->addSimpleMappings(array('uid','pass','mail','created','access','login','status','init'));
      $this->addFieldMapping('field_surname', 'field_apellidos_value');
      $this->addFieldMapping('field_name', 'field_nombre_value');
      $this->addFieldMapping('field_karma', 'field_karma_value');
      $this->addFieldMapping('field_ou', 'field_ou_value');
      // Dedupe names
      $this->addFieldMapping('name', 'name')
           ->dedupe('users', 'name');
      // File mapping
      $this->addFieldMapping('picture', 'picture')
           ->sourceMigration('UserPictures');
      // Roles mapping
      $this->addFieldMapping('roles')
           ->defaultValue(DRUPAL_AUTHENTICATED_RID); // Add innovador role
      $this->addFieldMapping('is_new') -> defaultValue('1');
    // Unmapped source fields


    // Unmapped destination fields
      $this->addUnmigratedDestinations(array(
        'role_names',
        'signature',
        'signature_format',
        'language',
        'timezone',
        'theme',
        'data',
        'field_ou:language',
        'field_name:language',
        'field_surname:language',
      ));

    $destination_fields = $this->destination->fields();
    if (isset($destination_fields['path'])) {
      $this->addFieldMapping('path')
           ->issueGroup(t('DNM'));
      if (isset($destination_fields['pathauto'])) {
        $this->addFieldMapping('pathauto')
             ->issueGroup(t('DNM'));
      }
    }
  }

  // Prepare row
  public function prepareRow($row) {

      // Prevent empty picture
      if (empty($row->picture)) 
         unset($row->picture);

      // Map karma value
      switch ( $row->field_karma_value ) {
        case '400' : 
          $row->field_karma_value = '500';
          break;
        case '1000' : 
          $row->field_karma_value = '1500';
          break;
      }
  
  }
}


/**
 * Concurso node migration
 */
class ConcursoMigration extends IdeasMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Concursos from Innova');

    // Database source connection
    $query = Database::getConnection('default', 'ono_innova_db')
              ->select('node', 'n');
    $query->leftjoin('field_data_body', 'fdb', 'fdb.entity_id = n.nid');
    $query->leftjoin('field_data_field_premios', 'fdfp', 'fdfp.entity_id = n.nid');
    $query->leftjoin('field_data_field_fecha_fin', 'fdfff', 'fdfff.entity_id = n.nid');
    //$query->leftjoin('field_data_field_winners', 'fdfw', 'fdfw.entity_id = n.nid');
    $query->leftjoin('field_data_field_image', 'fdfi', 'fdfi.entity_id = n.nid');
    $query->fields('n', array('nid','title','uid','status','created','changed','comment','sticky'))
          ->fields('fdb', array('body_value','body_summary'))
          ->fields('fdfp', array('field_premios_value'))
          ->fields('fdfff', array('field_fecha_fin_value'))
          //->fields('fdfw', array('field_winners_nid'))
          //->fields('fdfi', array('field_image_fid'))
          ->condition('type', 'concurso', '=');

    $this->source = new MigrateSourceSQL($query);

    // Set up destination - nodes of type idea
    $this->destination = new MigrateDestinationNode('contest');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'nid' => array(
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'Concruso ID.'
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // Mapped fields
    $this->addSimpleMappings(array('nid','title','status','created','changed','comment','sticky'));

    // References to users
    $this->addFieldMapping('uid', 'uid')
         ->sourceMigration('IdeaUser');

    $this->addFieldMapping('body', 'body_value');
    $this->addFieldMapping('field_awards', 'field_premios_value');
    // $this->addFieldMapping('field_winners', 'field_winners_nid')
    //      ->sourceMigration('IdeaNode');
    //$this->addFieldMapping('field_expiration_date', 'field_fecha_fin_value');
    $this->addFieldMapping('body:summary', 'body_summary');
    $this->addfieldMapping('body:format')->defaultValue('full_html');
    $this->addFieldMapping('is_new') -> defaultValue('1');
    // Unmapped destination fields
    $this->addUnmigratedDestinations(array(
      'promote',
      'revision',
      'log',
      'language',
      'translate',
      'tnid',
      'revision_uid',
      'field_expiration_date',
      'field_winners',
      'field_image',
        'field_image:file_class',
        'field_image:language',
        'field_image:preserve_files',
        'field_image:destination_dir',
        'field_image:destination_file',
        'field_image:file_replace',
        'field_image:source_dir',
        'field_image:urlencode',
        'field_image:title',
        'field_image:alt',
      'body:language',
      'field_awards:language',
      'field_contest_tags',
      'field_contest_tags:source_type',
      'field_contest_tags:create_term',
      'field_contest_tags:ignore_case',
    ));

    $destination_fields = $this->destination->fields();

    if (isset($destination_fields['path'])) {
      $this->addFieldMapping('path')
           ->issueGroup(t('DNM'));
      if (isset($destination_fields['pathauto'])) {
        $this->addFieldMapping('pathauto')
             ->issueGroup(t('DNM'));
      }
    }

  }


}


/**
 * Idea node migration
 */
class IdeaNodeMigration extends IdeasMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Ideas from Innova');


    // Database source connection
    $query = Database::getConnection('default', 'ono_innova_db')
              ->select('node', 'n');
    $query->leftjoin('field_data_field_description', 'fdfd', 'fdfd.entity_id = n.nid');
    $query->leftjoin('field_data_field_benefits', 'fdfb', 'fdfb.entity_id = n.nid');
    $query->leftjoin('field_data_field_concurso', 'fdfc', 'fdfc.entity_id = n.nid');
    //$query->leftjoin('field_data_field_idea_relatedusers', 'fdfiru', 'fdfiru.entity_id = n.nid');
  //  $query->leftjoin('workflow_node', 'wn', 'wn.nid = n.nid');
    $query->leftjoin('field_data_field_terminos_relacionados', 'fdftr', 'n.nid = fdftr.entity_id');
    $query->leftjoin('taxonomy_term_data', 'ttd', 'fdftr.entity_id = ttd.tid');
    $query->fields('n', array('nid','title','uid','status','created','changed','comment','sticky'))
          ->fields('fdfd', array('field_description_value'))
          ->fields('fdfb', array('field_benefits_value'))
          ->fields('fdfc', array('field_concurso_nid'))
          ->fields('ttd', array('vid'))
          //->fields('fdfiru', array('field_idea_relatedusers_uid'))
          //->fields('fdftr', array('field_terminos_relacionados_tid'))
        //  ->fields('wn', array('sid'))
          ->condition('type', 'idea', '=');
         // ->condition('ttd.vid', '3', '=');


    // Gives a single comma-separated list of related terms
    $query->groupBy('fdftr.entity_id');
    $query->addExpression('GROUP_CONCAT(fdftr.field_terminos_relacionados_tid)', 'terms');

    // By default, MigrateSourceSQL derives a count query from the main query -
    // but we can override it if we know a simpler way
    $count_query = db_select('node', 'n');
    $count_query->addExpression('COUNT(nid)', 'cnt');
    $count_query->condition('type', 'idea', '=');


    $this->source = new MigrateSourceSQL($query);

    // Set up destination - nodes of type idea
    $this->destination = new MigrateDestinationNode('idea');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'nid' => array(
          'type' => 'int',
          'not null' => TRUE,
          'description' => 'Idea ID.',
          'alias' => 'n'
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // Mapped fields
    $this->addSimpleMappings(array('nid','title','status','created','changed','comment','sticky'));

    // References to users
    $this->addFieldMapping('uid', 'uid')
        ->sourceMigration('IdeaUser');

    $this->addFieldMapping('field_contest', 'field_concurso_nid');

    // These are related terms, which by default will be looked up by name.
    $this->addFieldMapping('field_idea_tags', 'terms')
         ->separator(',')
         ->sourceMigration('IdeaTerm');

    $this->addFieldMapping('field_idea_tags:source_type')
         ->defaultValue('tid');
    $this->addFieldMapping('field_idea_tags:create_term')
         ->defaultValue(TRUE);
    $this->addFieldMapping('field_idea_tags:ignore_case')
         ->defaultValue(TRUE);

    $this->addFieldMapping('body', 'field_description_value');
    $this->addFieldMapping('field_benefits', 'field_benefits_value');

    $this->addFieldMapping('is_new') -> defaultValue('1');

    // Unmapped destination fields
    $this->addUnmigratedDestinations(array(
      'promote',
      'revision',
      'log',
      'language',
      'tnid',
      'revision_uid',
      'field_related_users',
      'body:summary',
      'body:format',
      'body:language',
      'field_benefits:language',
      'field_attachments',
        'field_attachments:file_class',
        'field_attachments:language',
        'field_attachments:preserve_files',
        'field_attachments:destination_dir',
        'field_attachments:destination_file',
        'field_attachments:file_replace',
        'field_attachments:source_dir',
        'field_attachments:urlencode',
        'field_attachments:description',
        'field_attachments:display',
      'field_status',
      'field_contest_tags',
        'field_contest_tags:source_type',
        'field_contest_tags:create_term',
        'field_contest_tags:ignore_case',
      'field_workflow_closing_reason',
        'field_workflow_closing_reason:source_type',
        'field_workflow_closing_reason:create_term',
        'field_workflow_closing_reason:ignore_case',
      'field_workflow_closing_area',
        'field_workflow_closing_area:source_type',
        'field_workflow_closing_area:create_term',
        'field_workflow_closing_area:ignore_case',
      'field_vote',
        'field_vote:target'
    ));

    $destination_fields = $this->destination->fields();

    if (isset($destination_fields['path'])) {
      $this->addFieldMapping('path')
           ->issueGroup(t('DNM'));
      if (isset($destination_fields['pathauto'])) {
        $this->addFieldMapping('pathauto')
             ->issueGroup(t('DNM'));
      }
    }

  }


}

/**
 * Import items from the migrate_example_beer_comment table and make them into
 * Drupal comment objects.
 */
class IdeaCommentMigration extends IdeasMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = 'Comments about beers';

    // Database source connection
    $query = Database::getConnection('default', 'ono_innova_db')
            ->select('comment', 'c');

    $query->leftjoin('node', 'n', 'n.nid = c.nid');
    $query->leftjoin('field_data_comment_body', 'fdcb', 'fdcb.entity_id = c.nid');
   
    $query->fields('c', array('cid', 'pid', 'nid', 'uid', 'subject', 'hostname', 'created', 'changed','status','thread','name','mail'))
          ->fields('fdcb', array('comment_body_value'))
          ->condition('n.type', 'idea', '=')
          ->orderBy('pid', 'ASC');


    $this->source = new MigrateSourceSQL($query);
    // Note that the machine name passed for comment migrations is
    // 'comment_node_' followed by the machine name of the node type these
    // comments are attached to.
    $this->destination =
      new MigrateDestinationComment('comment_node_idea');

    $this->map = new MigrateSQLMap($this->machineName,
      array('cid' => array(
            'type' => 'int',
            'not null' => TRUE,
           )
         ),
      MigrateDestinationComment::getKeySchema()
    );

    // Mapped fields
    $this->addSimpleMappings(array('created','changed', 'subject', 'name','mail','hostname','thread'));
    $this->addFieldMapping('status')
         ->defaultValue(COMMENT_PUBLISHED);

    $this->addFieldMapping('nid', 'nid')
         ->sourceMigration('IdeaNode');

    $this->addFieldMapping('uid', 'uid')
         ->sourceMigration('IdeaUser')
         ->defaultValue(0);

    $this->addFieldMapping('pid', 'pid')
         ->sourceMigration('IdeaComment')
         ->description('Parent comment.');

    $this->addFieldMapping('comment_body', 'comment_body_value');

    // No unmapped source fields

    // Unmapped destination fields
    $this->addUnmigratedDestinations(array(
      'comment_body:format', 'comment_body:language',
      'homepage',
      'language', 
    ));

    $destination_fields = $this->destination->fields();
    if (isset($destination_fields['path'])) {
      $this->addFieldMapping('path')
           ->issueGroup(t('DNM'));
      if (isset($destination_fields['pathauto'])) {
        $this->addFieldMapping('pathauto')
             ->issueGroup(t('DNM'));
      }
    }
  }
}
