<?php
/**
 * @file
 * Declares our migrations.
 */

/**
 * Implements hook_migrate_api().
 */
function migrate_onoinnova_migrate_api() {
  // Usually field mappings are established by code in the migration constructor -
  // a call to addFieldMapping(). They may also be passed as arguments when
  // registering a migration - in this case, they are stored in the database
  // and override any mappings for the same field in the code. To do this,
  // construct the field mapping object and configure it similarly to when
  // you call addFieldMapping, and pass your mappings as an array below.
  $translate_mapping = new MigrateFieldMapping('translate', NULL);
  $translate_mapping->defaultValue(0);

  $api = array(
    'api' => 2,
    'groups' => array(
      'idea' => array(
        'title' => t('ONO Innova Ideas'),
      ),
      'news' => array(
        'title' => t('ONO Innova News'),
      ),
    ),

    'migrations' => array(

      // IDEAS
      'Concurso' => array(
        'class_name' => 'ConcursoMigration',
        'group_name' => 'idea',
        // 'dependencies' => array(
        //   'IdeaUser',
        // ),
      ),
      'IdeaTerm' => array(
        'class_name' => 'IdeaTermMigration',
        'group_name' => 'idea',
      ),
      'UserPictures' => array(
        'class_name' => 'UserPicturesMigration',
        'group_name' => 'idea',
      ),
      'IdeaUser' => array(
        'class_name' => 'IdeaUserMigration',
        'group_name' => 'idea',
        'dependencies' => array(
          'UserPictures',
        ),
      ),
      'IdeaNode' => array(
        'class_name' => 'IdeaNodeMigration',
        'group_name' => 'idea',
        // You may optionally declare dependencies for your migration - other
        // migrations which should run first. In this case, terms assigned to our
        // nodes and the authors of the nodes should be migrated before the nodes
        // themselves.
        // 'dependencies' => array(
        //   'IdeaTerm',
        //   'IdeaUser',
        //   'Concurso',
        // ),
        // Here is where we add field mappings which may override those
        // specified in the group constructor.
        'field_mappings' => array(
          $translate_mapping,
        ),
      ),
      'IdeaComment' => array(
        'class_name' => 'IdeaCommentMigration',
        'group_name' => 'idea',
        // 'dependencies' => array(
        //   'IdeaUser',
        //   'IdeaNode',
        // ),
      ),
      // 'ConcursoComment' => array(
      //   'class_name' => 'ConcursoCommentMigration',
      //   'group_name' => 'idea',
        // 'dependencies' => array(
        //   'IdeaUser',
        //   'IdeaNode',
        // ),
      //),

      // NEWS

      'NewsVPictures' => array(
        'class_name' => 'NewsVPicturesMigration',
        'group_name' => 'news',
      ),
      // 'NewsHPictures' => array(
      //   'class_name' => 'NewsHPicturesMigration',
      //   'group_name' => 'news',
      // ),
      'NewsNode' => array(
        'class_name' => 'NewsNodeMigration',
        'group_name' => 'news',
      ),
      
      'NewsComment' => array(
        'class_name' => 'NewsCommentMigration',
        'group_name' => 'news',
      ),
      
    ),
  );
  return $api;
}
?>