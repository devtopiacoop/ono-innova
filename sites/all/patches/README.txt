This directory is created for custom patches over the core
or contributed modules.

This file includes the description and the reasons why is
required the patch.

Remember to apply the patches with command:

git apply path-to-patch/patch-namefile.patch

----------------------------------------------------------
user-fix-operand-types-error.patch
----------------------------------------------------------
On the "full" view mode of the user display suite we are
tabbing 2 views:
- Tab 1: Flagged ideas from URL user UID
- Tab 2: Authored ideas from URL user UID
Tab 2 is generating a Fatal error:
Fatal error: Unsupported operand types in modules/user/user.module 
on line 2646

After some investigations we've found:
- Adding relationship "Flag:node" on view, with "Include only flagged
content" skips the error and works renderization, but the view results
are not the desired
- Changing output renderization to "Fields" instead "Display Suite"
works properly, but it would break the standard presentation on the
project
- It looks a PHP error due to array/objects handling 
- The error it's not reported on Drupal community

----------------------------------------------------------
message-bad_operand-3.patch
----------------------------------------------------------
Bug reported on Messages module:
https://www.drupal.org/node/2166155