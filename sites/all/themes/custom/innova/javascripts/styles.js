Drupal.behaviors.innovaTheme = {
  attach: function (context, settings) {
    jQuery('.view-id-suscripciones.view-display-id-block .views-row .views-field-ops a.flag-link-toggle').text('');

    jQuery('.page-preguntas-frecuentes .view-faq .view-content h2').on('click', function (ev) {
      ev.preventDefault();
      var element = jQuery(this);

      element.toggleClass('minus-icon'); // H2
      element.parent().find('.field-type-text-with-summary').slideToggle('fast');
    });

    jQuery('.multi-user-toggle').on('click', function (ev) {
      ev.preventDefault();

      var _this = jQuery(this);
      var triggerLink = _this.find('a');
      var ideaMembers = _this.closest('.field-item').find('.author-idea-members');

      _this.toggleClass('active');
      ideaMembers.toggle();

      if (_this.hasClass('active')) {
        triggerLink.text('Ocultar miembros');
      } else {
        triggerLink.text('Ver miembros')
      }
    });

    jQuery('a.dropdown-toggle').on('click', function () {
      window.location.href = jQuery(this).attr('href');
    });

    jQuery('.view-id-goals_completed_by_user .col').on('mouseenter', function(){
      _this = jQuery(this);
      _hoverElement = jQuery(this).find('.views-field-nothing');

      _hoverElement.css('top', (_hoverElement.innerHeight() - _this.innerHeight()) / 2 * -1).css('left', _this.innerWidth());
    });

    jQuery('body.page-node-add-idea .field-name-field-related-users .glyphicon-refresh')
      .removeClass('glyphicon-refresh')
      .addClass('glyphicon-remove')
      .on('click', function(){
        jQuery(this).parent().parent().find('input').val('');
      });

    (function() {
      var messageBox = jQuery('body.page-confirmacion-idea-creada .alert.alert-block');

      messageBox.find('li').each(function(){
        var text = jQuery(this).text();

        if(text.indexOf('tienes un total') != -1){
          var confirmText = '<div class="confirmation-points-text">' + text + '</div>';
          jQuery('.field-name-idea-confirm-header').append(confirmText);
        }
      });

      messageBox.remove();
    })();

    (function() {
      var _parent = jQuery('header.navbar-default .region-header-actions');

      if (_parent.children().length == 1) {
        _parent.children().css('float', 'right').css('display', 'block');
      }
    })();

    (function() {
      jQuery('.field-group-htabs .pagination a').on('click', function(ev){
        var wrapper = jQuery('.field-group-htabs');
        var selected = wrapper.find('.horizontal-tab-button.selected a').attr('href');

        if(selected !== undefined && selected != '#undefined'){
          ev.preventDefault();
          window.location.href = jQuery(this).attr('href') + selected;
        }
      });
    })();

    adaptRhombusSize();
  }
};

function adaptRhombusSize(){
  // var totalDocumentHeight = 0;
  var rhombus = jQuery('.rhombus');

  /*jQuery('body > *:visible').each(function(){
    totalDocumentHeight += jQuery(this).height();
  });

  var rhombusHeight = totalDocumentHeight + Math.abs(parseInt(rhombus.css('top'))) - 100;

  if(jQuery('body').height() > 1000){
    rhombusHeight -= 300;
  }

  if(navigator.userAgent.match(/MSIE 8\.0/i) !== null){
    rhombus.css('height', jQuery(window).height() + Math.abs(parseInt(rhombus.css('top'))) - 100);
  } else {
    rhombus.css('height', rhombusHeight);
  } */

  rhombus.css('height', jQuery(window).height() + Math.abs(parseInt(rhombus.css('top'))) - 100);
}

function equalizeIdeaTeasers(element) {
  var _this = jQuery(element);
  var userCard = _this.find('.user-card');
  var ideaInfo = _this.find('.idea-info');
  var ideaMembers = _this.find('.author-idea-members');

  if (userCard.height() < ideaInfo.innerHeight()) {
    var authoredBlock = userCard.find('.field-name-innova-authored-block');
    var authoredMargin = isNaN(parseInt(authoredBlock.css('margin-bottom'))) ? 0 : parseInt(authoredBlock.css('margin-bottom'));

    authoredBlock.css('margin-bottom', authoredMargin + ideaInfo.innerHeight() - userCard.height());
  } else if (ideaInfo.innerHeight() < userCard.height()) {
    ideaInfo.css('height', userCard.innerHeight());
  }

  if (ideaMembers.length > 0) {
    ideaMembers.css('left', userCard.innerWidth());
    ideaMembers.css('width', ideaInfo.innerWidth());
    ideaMembers.css('height', ideaInfo.innerHeight());

    ideaMembers.find('li').css('height', parseInt(ideaInfo.innerHeight()) / 2);
  }
}

if (typeof String.prototype.trim !== 'function') {
  String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, '');
  }
}