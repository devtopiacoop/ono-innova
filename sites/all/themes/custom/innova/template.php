<?php

/**
 * @file
 * template.php
 */

/**
 * Implements template_preprocess()
 *
 * @param $variables
 */
function innova_preprocess(&$variables) {
  // Renders the search form and puts it in a variable to use in the templates
  $search_box              = drupal_get_form('search_form');
  $variables['search_box'] = drupal_render($search_box);

  // Adds a variable with the absolute path to the theme dir
  $variables['theme_path'] = base_path() . drupal_get_path('theme', 'innova');

  // Renders the idea states block
  if (drupal_is_front_page()) {
    $idea_states              = module_invoke('innova_display', 'block_view', 'innova_ideas_states_summary');
    $variables['idea_states'] = render($idea_states['content']);
  }
}

/**
 * Implements theme_preprocess_field()
 *
 * @param $variables
 */
function innova_preprocess_field(&$variables) {
  if (isset($variables['element']['#field_name']) && in_array(
      $variables['element']['#field_name'],
      array(
        'body',
        'field_benefits',
        'field_idea_tags',
        'field_contest_tags',
        'field_awards'
      ))
  ) {
    $variables['theme_hook_suggestions'][] = "field__field_generic_no_colon";
  }

  if (isset($variables['element']['#field_name']) && in_array(
      $variables['element']['#field_name'],
      array(
        'innova_user_followers_followed',
        'innova_user_ranking',
        'innova_user_points_count',
        'flag_usernode_follow_idea',
        'flag_usernode_like_idea',
        'innova_user_goals_count'
      ))
  ) {
    $variables['theme_hook_suggestions'][] = "field__field_generic_no_colon_contained";
  }
}

/**
 * Implements theme_preprocess_page()
 *
 * This method is used to include a breakpoint in the end just to check which theme suggestion come through
 *
 * @param $variables
 */
function innova_preprocess_page(&$variables) {
  if (
    in_array(arg(0), array('ranking', 'ranking-ideas', 'preguntas-frecuentes', 'mis-medallas')) ||
    isset($variables['node']->type) && in_array($variables['node']->type, array('page', 'news', 'idea'))
  ) {
    $variables['title'] = FALSE;
  }

  if (!empty($variables['page']['content']['system_main']['#entity_type']) && $variables['page']['content']['system_main']['#entity_type'] == 'user' && user_is_logged_in()) {
    $variables['theme_hook_suggestions'][] = "page__full_profile_view";
  }

  if (isset($variables['node'])) {
    // If the node type is "blog_madness" the template suggestion will be "page--blog-madness.tpl.php".
    $variables['theme_hook_suggestions'][] = 'page__type_' . $variables['node']->type;
  }
}

/**
 * Implements template_form_alter()
 *
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function innova_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_form') {
    // Removes the search button added by the parent theme
    unset($form['basic']['keys']['#theme_wrappers']);
    unset($form['basic']['keys']['#attributes']['placeholder']);

    $form['basic']['keys']['#prefix'] = '<span class="fa fa-search"></span>';
  }
}

/**
 * Implements template_js_alter()
 *
 * Updates JS version from the default to 1.9.1 when using our theme
 *
 * @param $javascript
 */
function innova_js_alter(&$javascript) {
  $jquery_path = drupal_get_path('theme','innova') . '/javascripts/jquery-1.9.1.min.js';

  // We duplicate the important information from the Drupal version and update the version
  $javascript[$jquery_path] = $javascript['misc/jquery.js'];
  $javascript[$jquery_path]['version'] = '1.9.1';
  $javascript[$jquery_path]['data'] = $jquery_path;

  // Then we remove the Drupal core version
  unset($javascript['misc/jquery.js']);
}

/**
 * Implements template_preprocess_region().
 */
function innova_preprocess_region(&$variables) {
  // Adds custom classes to our regions
  if ($variables['region'] == 'sub_footer') {
    $variables['classes_array'][] = 'container';
  }
}

/**
 * Implements template_preprocess_views_view
 *
 * @param $vars
 */
function innova_preprocess_views_view(&$vars) {
  if ($vars['view']->name == 'quotes' && $vars['view']->current_display == 'block') {
    // Adds a bootstrap class to the footer view slideshow
    $vars['classes_array'][] = 'container';
  }

  // Hides the "Otras ideas de este usuario" block when the idea has more than one author
 /* if ($vars['view']->name == 'ideas' && $vars['view']->current_display == 'block_7') {
    $node = menu_get_object();

    if (isset($node->field_related_users[LANGUAGE_NONE]) && count($node->field_related_users[LANGUAGE_NONE]) > 0) {
      $vars['rows']  = '';
      $vars['empty'] = '';
    }
  }*/
}

/**
 * Implements template_preprocess_block
 *
 * @param $variables
 */
function innova_preprocess_block(&$variables) {
  if ($variables['block']->module == 'menu' && $variables['block']->delta == 'menu-footer-menu') {
    $variables['classes_array'][] = 'col-md-9';
  }

  if ($variables['block']->module == 'block' && $variables['block']->bid == '97') {
    $variables['classes_array'][] = 'col-md-3';
  }
}

/**
 * @param $variables
 *
 * @return string

function innova_menu_tree__menu_footer_menu(&$variables) {
 * // Adds bootstrap classes to the footer navigation
 * return '<ul class="menu nav">' . $variables['tree'] . '</ul>';
 * }*/