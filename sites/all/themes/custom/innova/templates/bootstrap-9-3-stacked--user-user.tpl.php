<?php
/**
 * @file
 * Bootstrap 9-3 stacked template for Display Suite.
 */
?>

<<?php print $layout_wrapper . $layout_attributes; ?> class="<?php print $classes; ?>">
  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <div class="user-header">
    <div class="container">
      <div class="row">
        <<?php print $header_wrapper; ?> class="col-md-12 <?php print $header_classes; ?>">
          <?php print $header; ?>
        </<?php print $header_wrapper; ?>>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <<?php print $left_wrapper; ?> class="col-md-9 <?php print $left_classes; ?>">
        <?php print $left; ?>
      </<?php print $left_wrapper; ?>>

      <<?php print $right_wrapper; ?> class="col-md-3 user-sidebar <?php print $right_classes; ?>">
        <?php print $right; ?>
      </<?php print $right_wrapper; ?>>
    </div>

    <div class="row">
      <<?php print $footer_wrapper; ?> class="col-md-12 <?php print $footer_classes; ?>">
      <?php print $footer; ?>
    </<?php print $footer_wrapper; ?>>
    </div>
  </div>
</<?php print $layout_wrapper ?>>
<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
