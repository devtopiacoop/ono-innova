## Compilar SASS a CSS

Para compilar SASS a CSS necesitas tener Ruby instalado, si no lo tienes puedes seguir las siguientes instrucciones para hacerlo en OS X a través de RVM: [http://foffer.dk/install-ruby-on-os-x-10-10-yosemite-using-rvm/](http://foffer.dk/install-ruby-on-os-x-10-10-yosemite-using-rvm/)

Una vez tengas Ruby instalado necesitas instalar la gema bundler con el comando `gem install bundler`, esta gema la usaremos para instalar todas las dependencias del theme.

Con todas las dependencias basta con acceder a la carpeta del theme y ejecutar el comando `bundle install`, que instalará las gemas en las versiones necesarias para poder compilar el theme.

Dentro de esta misma carpeta, con el comando `bundle exec compass watch` compilaremos los SASS a CSS, con `bundle exec compass watch` iniciaremos el proceso que compilará automáticamente al detectar cambios en las fuentes.
