<?php

function ds_bootstrap_3_9() {
  return array(
    'label' => t('Bootstrap Two Columns: 3-9'),
    'regions' => array(
      'left' => t('Left'),
      'right' => t('Right'),
    ),
    'form' => TRUE,
    'image' => FALSE,
  );
}