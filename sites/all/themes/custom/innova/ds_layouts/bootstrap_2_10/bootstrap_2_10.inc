<?php

function ds_bootstrap_2_10() {
  return array(
    'label' => t('Bootstrap Two Columns: 2-10'),
    'regions' => array(
      'left' => t('Left'),
      'right' => t('Right'),
    ),
    'form' => TRUE,
    'image' => FALSE,
  );
}