<?php

function ds_bootstrap_4_8() {
  return array(
    'label' => t('Bootstrap Two Columns: 4-8'),
    'regions' => array(
      'left' => t('Left'),
      'right' => t('Right'),
    ),
    'form' => TRUE,
    'image' => FALSE,
  );
}