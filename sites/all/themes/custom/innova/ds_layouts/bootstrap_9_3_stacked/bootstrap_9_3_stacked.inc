<?php

function ds_bootstrap_9_3_stacked() {
  return array(
    'label' => t('Bootstrap Two Columns: 9-3 Stacked'),
    'regions' => array(
      'header' => t('Header'),
      'left' => t('Left'),
      'right' => t('Right'),
      'footer' => t('Footer'),
    ),
    'form' => TRUE,
    'image' => FALSE,
  );
}